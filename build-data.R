rm(list=ls())

# Number of nodes in the graph
InitN <- N <- 40

breaks<-c(20,30,60,100)  # The actual change points

# time and node labels
y<-c(rep(1, times=19), rep(2,times=10), rep(3, times=30), rep(4,times=41))
z<-c(rep(1, times=5), rep(2,times=5), rep(3, times=10), rep(4, times=20))

Mtx<-list()
Mtx[[1]]<-diag(c(1,1,1,1), nrow=4, ncol=4)
Mtx[[2]]<-t(matrix(c(0,0,1,1,0,0,0,0,1,0,1,0,1,0,0,0),nrow=4, ncol=4))
Mtx[[3]]<-t(matrix(c(0,1,0,0,1,0,0,0,0,0,0,1,0,0,1,0), nrow=4, ncol=4))
Mtx[[4]]<-t(matrix(c(1,0,0,1,0,1,0,1,0,0,1,1,1,1,1,0), nrow=4, ncol=4))

# simulates the membership (e.g) of i in A (pr(i in A_k)=omega_k) 
find.segment<-function(v, u){
  i<-1
  while (v[i]<u) i=i+1
  return(i)
}

n_edges<-1000

Gnu<-matrix(0, nrow=3, ncol = n_edges)
counter=1
while(counter<= n_edges){
i<-ceiling(runif(1, min=0, max=N))
nu<-runif(1, min = 0, max=100)
d<-find.segment(breaks, nu)
SpeakTo<-which(Mtx[[d]][z[i], ]!=0)   
if (length(SpeakTo)==1){
      store<-which(z==SpeakTo)
      init<-min(store)-1
      end<-max(store)
      j<-ceiling(runif(1, min=init, max=end))
      while (j==i) j<-ceiling(runif(1, min=init, max=end))
      Gnu[,counter]<-c(nu,i,j)
      counter<-counter+1
}
else if (length(SpeakTo)>1){
      store<-which((z %in% SpeakTo)==TRUE)
      end<-length(store)
      tmp<-ceiling(runif(1,0,end))
      j<-store[tmp]
      while (j==i){
        tmp<-ceiling(runif(1,0,end))
        j<-store[tmp]
      }
      Gnu[,counter]<-c(nu,j,i) 
      counter<-counter+1
}
else {}
}
    
str<-which(Gnu[1,]==0);
if (length(str)!=0) print("warning")
#sorting
tmp<-sort(Gnu[1,])
TakeIn<-match(tmp, Gnu[1,])
Gnu<-Gnu[,TakeIn]
    
# undirected interactions (i<j) !!
store<-which(Gnu[2,]>Gnu[3,]) 
Gnu[,store]<-Gnu[c(1,3,2), store]
    
# noising data
taille<-ncol(Gnu);
seuil<-round(.33*taille)
Gnu.copy<-Gnu;
    
mylist<-c()
while (length(mylist)<=seuil){
  tmp<-ceiling(runif(1, 0, (ncol(Gnu.copy))))
  mylist<-c(mylist, Gnu.copy[1,tmp])
  Gnu.copy<-Gnu.copy[,-tmp]
}
    
TakeIn<-match(mylist, Gnu[1,])
for (i in 1:length(TakeIn)){
    at<-TakeIn[i]
    Gnu[3,at]<-ceiling(runif(1,0,N))
    while (Gnu[3,at]==Gnu[2,at]) Gnu[3,at]<-ceiling(runif(1,0,N))
}
    
# making interactions undirected after sampling
store<-which(Gnu[2,]>Gnu[3,]) 
Gnu[,store]<-Gnu[c(1,3,2), store]


save(Gnu, file="~/CpDyna/data/Gnu.RData")
