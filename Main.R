library(mclust)
library(cluster)
library(reshape2)
#library(CpDyna)
library(igraph)
library(RColorBrewer)

current_dir <- "/home/marco/Documenti/PostDoc/"  # this is the directory where the folder "package" is located
Rcpp::sourceCpp(paste(current_dir,"package/MinimalPartition.cpp",sep = ""))
Rcpp::sourceCpp(paste(current_dir,"package/RegularPartition.cpp",sep = ""))


## Utility functions used to produce different initializations
EqualTo<-function(v1, v2) return((adjustedRandIndex(v1,v2)==1)) 
CleanLines<-function(M){
  i1<-1
  while (i1<nrow(M)){
    i2<-i1+1
    ToClean<-c()
    while(i2<=nrow(M)){
      if (EqualTo(M[i1,], M[i2,])) {
        ToClean<-c(ToClean, i2)
        i2<-i2+1
      }
      else i2<-i2+1
    }
    if (!is.null(ToClean)) M<-M[-ToClean,]
    i1<-i1+1
    if (!is.matrix(M)) break
  }
  return(M)
}

# Main function to fit the model to data and (possibly) performing model selection
ModFit <- function(Gnu,
                   kmin,
                   kmax,
                   eps = 10^(-9),
                   iter.vem = 50,
                   N_initializations = 1,
                   MinimalPartition = TRUE,
                   custom_partition = NULL, 
                   verbose = FALSE
){
  InitN <- N <- max(max(Gnu[2,]), max(Gnu[3,]))    # for the moment InitN and N are the same thing.. 
  M <- ncol(Gnu)
  if (MinimalPartition==TRUE){
    X <- array(0, dim = c(InitN,InitN,M))
    for (i in 1:M) X[as.integer(Gnu[2,i]), as.integer(Gnu[3,i]), i]<-X[as.integer(Gnu[3,i]), as.integer(Gnu[2,i]), i]<-1
  }
  else {
    ptn <- custom_partition
    X <- GnuToX(EGnu=Gnu, Eptn = ptn, N = InitN)
  }
  tmp_D<-c()
  tmp_LB<-c()
  
  # Init (Fabrice k-means)
  # Building an initial partition (ipart) possibly having nothing in common with custom partition. This partition
  # will be used for initialization purposes only
  istep <-(Gnu[1,M] - Gnu[1,1])/100
  ipart <- seq(from = Gnu[1,1], to = Gnu[1,M], by = istep)
  X_ <- GnuToX(EGnu = Gnu, Eptn=ipart, N=InitN)
  X2<-X_
  dim(X2)<-c(InitN, InitN*length(ipart))
  lock_tau <- vector("list", length = kmax)
  lock_ptn <- vector("list", length = kmax)
  lock_Lbd <- vector("list", length = kmax)
  lock_pi <- vector("list", length = kmax)
  ## loop in K
  for (K in kmin:kmax){
    if (verbose == TRUE) cat(" testing for number of clusters K equal to: ", K, "\n")
    if (K==1){ 
      inz<-rep(1, times=N)
      tau<-matrix(0, N, K)  
      for (k in 1:K){
        store<-which(inz==k)
        tau[store,k]<-1
      }
      tau[tau<.Machine$double.xmin] <- .Machine$double.xmin
      
      # firt Maximization step
      if (MinimalPartition==TRUE){
        out<-VM_MP(Etau =tau, ptn=Gnu[1,], EGnu = Gnu)
        out$Lbd[out$Lbd<.Machine$double.xmin]<- .Machine$double.xmin
        LB<-LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
        if (verbose==TRUE) print(LB)
      }
      if (MinimalPartition==FALSE){
        out<-VM_(Etau = tau, ptn=ptn, EX=X)
        out$Lbd[out$Lbd<.Machine$double.xmin]<-.Machine$double.xmin
        LL<-log(out$Lbd)
        LB<-LowerBound_(tau, X, out$ptn[-1], out$Pi, out$Lbd,LL,
                        ValuesAtPtn = ptn[out$ptn])
        if (verbose==TRUE) cat(LB)
      }
      
      counter<-0
      old_LB=LB-1
      while (abs(LB-old_LB)>eps && counter<iter.vem){
        if (LB - old_LB < 0.001) print("R::warning: decreasing lower bound!!!")
        if (LB == .Machine$double.xmin) print("R::Warning: NaN LB")
        counter <- counter+1
        old_LB <- LB
        store_pi <- out$Pi
        
        # variational Expectation
        Iptn<-out$ptn[-1]  # initial changepoints
        ID<-length(Iptn)   # initial D
        IL<-out$Lbd    
        ipi<-out$Pi
        
        if (MinimalPartition==FALSE){
          tau<-VE_(tau, Iptn,ValuesAtPtn = Iptn, ipi, IL, X)
          tau[tau<.Machine$double.xmin] <-.Machine$double.xmin
          LB<-LowerBound_(tau, X, out$ptn[-1], out$Pi, out$Lbd, LL,ValuesAtPtn = ptn[out$ptn])
          if (verbose == TRUE) print(LB)
          
          # Variational Maximization
          out<-VM_(Etau = tau, ptn=ptn, EX = X)
          out$Lbd[out$Lbd<.Machine$double.xmin]<-.Machine$double.xmin
          LL<-log(out$Lbd)
          LB<-LowerBound_(tau, X, out$ptn[-1], out$Pi, out$Lbd,LL,ValuesAtPtn = ptn[out$ptn])
          if (verbose == TRUE) print(LB)
        }
        if (is.nan(LB)) LB<-.Machine$double.xmin
        if (MinimalPartition==TRUE){
          # No Variational Expetctation: there is a single cluster
          tau[tau<.Machine$double.xmin] <- .Machine$double.xmin
          # Variational Maximization
          out<-VM_MP(Etau = tau, ptn=Gnu[1,], EGnu = Gnu)
          out$Lbd[out$Lbd<.Machine$double.xmin]<- .Machine$double.xmin
          LB<-LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
          if (verbose == TRUE) print(LB)
        }
      }
      if (MinimalPartition==TRUE) tmpo<-LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
      else{
        LL<-log(out$Lbd)
        tmpo<-LowerBound_(tau,X, out$ptn[-1], out$Pi, out$Lbd,LL, ValuesAtPtn = ptn[out$ptn])
      }
      
      # stocking the estimated values of D and the lower bound for K=1
      tmp_D<-c(tmp_D, length(out$ptn[-1]))
      tmp_LB<-c(tmp_LB, tmpo)
      
      # stocking tau, the estimated changepoints and the model parameters
      lock_tau[[K-kmin+1]] <- tau
      lock_ptn[[K-kmin+1]] <- out$ptn
      lock_Lbd[[K-kmin+1]] <- out$Lbd
      lock_pi[[K-kmin+1]] <- out$Pi
    }
    else {
      # we produce 10 different initializations with the spectral clustering
      INz<-matrix(nrow=N_initializations, ncol=N)
      for (ctr in 1:N_initializations) INz[ctr, ]<-kmeans(X2, K)$cluster
      # I am selecting the equivalent (due to label switching) initial taus
      INz<-CleanLines(INz) 
      if (!is.matrix(INz)) INz<-t(as.matrix(INz))
      # I am running the algorithm for each initialization
      Intau<-vector("list", length = nrow(INz))
      Inout<-vector("list", length = nrow(INz))
      InLB<-vector(length=nrow(INz))
      # Running VEM for each initialization
      for (RowCounter in 1:nrow(INz)){
        inz<-INz[RowCounter,]
        ## init tau
        tau<-matrix(0, N, K)  
        for (k in 1:K){
          store<-which(inz==k)
          tau[store,k]<-1
        }
        tau[tau<.Machine$double.xmin] <- .Machine$double.xmin
        
        ## first M step
        if (MinimalPartition==FALSE) out<-VM_(Etau = tau, ptn=ptn, EX=X)
        else out<-VM_MP(Etau = tau, ptn=Gnu[1,], EGnu = Gnu)
        out$Lbd[out$Lbd<.Machine$double.xmin]<-.Machine$double.xmin
        LL<-log(out$Lbd)
        if (MinimalPartition==FALSE) LB<-LowerBound_(tau, X, out$ptn[-1], out$Pi, out$Lbd,LL,
                                                     ValuesAtPtn = ptn[out$ptn])
        else LB <- LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
        if (verbose == TRUE) cat("M step: ", LB, "\n")
        
        ## VEM loop
        counter<-0
        old_LB=LB-1
        while (abs(LB-old_LB)>eps && counter< iter.vem){
          print(counter)
          if (LB<old_LB && LB!=.Machine$double.xmin) { 
            print("R::warning: decreasing lower bound !!!")
          }
          if (LB==.Machine$double.xmin) print("R::Warning: NaN LB")
          counter<-counter+1
          old_LB<-LB
          store_pi<-out$Pi
          
          # Variational E step
          Iptn<-out$ptn[-1]
          ID<-length(Iptn)
          IL<-out$Lbd
          ipi<-out$Pi
          if (MinimalPartition==FALSE) tau <- VE_(tau, Iptn, ValuesAtPtn = ptn[Iptn], ipi, IL, X)
          if (MinimalPartition==TRUE) tau <- VE_MP(tau, Iptn, ipi, IL, X, Gnu)
          tau[tau<.Machine$double.xmin] <-.Machine$double.xmin;
          if (MinimalPartition==FALSE) LB<-LowerBound_(tau, X, out$ptn[-1], out$Pi, out$Lbd, LL,ValuesAtPtn = ptn[out$ptn])
          else LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
          if (verbose==TRUE) cat("E step: ",LB, "\n")
          
          # Variational M step
          if (MinimalPartition==FALSE) out<-VM_(Etau = tau, ptn=ptn, EX = X)
          else out <- VM_MP(tau, ptn=Gnu[1,], EGnu = Gnu)
          out$Lbd[out$Lbd<.Machine$double.xmin]<-.Machine$double.xmin
          LL<-log(out$Lbd)
          if (MinimalPartition == FALSE) LB<-LowerBound_(tau, X, out$ptn[-1], out$Pi, out$Lbd,LL,ValuesAtPtn =ptn[out$ptn])
          else LB <- LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
          if (is.nan(LB)){
            print(c("Warning: NaN lower bound "))
            LB<-.Machine$double.xmin
          }
          if (verbose == TRUE) cat("M step: ",LB, "\n")
        }
        
        ## Last lower bound
        LL<-log(out$Lbd)
        if (MinimalPartition==FALSE) tmpo<-LowerBound_(tau,X, out$ptn[-1], out$Pi, out$Lbd,LL, ValuesAtPtn = ptn[out$ptn])
        else tmpo <- LowerBound_MP(tau, Gnu, out$ptn, out$Pi, out$Lbd)
        ## I'm saving what needed
        Intau[[RowCounter]]<-tau
        Inout[[RowCounter]]<-out
        InLB[RowCounter]<-tmpo
      }
      BestInit<-which.max(InLB) 
      tmp_D <- c(tmp_D, length(Inout[[BestInit]]$ptn[-1]))
      tmp_LB <- c(tmp_LB, InLB[BestInit])
      #
      lock_tau[[K - kmin + 1]] <- Intau[[BestInit]]
      lock_ptn[[K - kmin + 1]] <- Inout[[BestInit]]$ptn
      lock_Lbd[[K - kmin + 1]] <- Inout[[BestInit]]$Lbd
      lock_pi[[K - kmin +1]] <- Inout[[BestInit]]$Pi
    }
  }
  
  Kcount <- which.max(tmp_LB)              # selecting the best run
  estK <- Kcount + kmin - 1                # corresponding number of clusters
  estD <- tmp_D[Kcount]                    # corresponding segmentation
  estLbd <- lock_Lbd[[Kcount]]             # correspondint Lambda
  estPi <- lock_pi[[Kcount]]               # corresponding Pi
  N <- nrow(lock_tau[[Kcount]])
  
  # Building outz (the estimated z) 
  newMM<-MM <- matrix(0, nrow=N, ncol=estK)
  max_tau<-apply(lock_tau[[Kcount]], 1, max)
  MM[which(lock_tau[[Kcount]]==max_tau, arr.ind=1)] <- 1
  for (k in 1:estK) newMM[,k]<-k*MM[,k]
  outz<-apply(newMM, 1, sum)
  ## managing the inactive nodes if any
  #     if (length(inactive.nodes!=0)){
  #       tmp1<-seq(1:InitN);
  #       tmp2<-match(tmp1, WhoIsWho);
  #       tmp2[inactive.nodes]<-1;
  #       tmp2[WhoIsWho]<-outz
  #       outz<-tmp2
  #     }
  
  # Estimated change points
  tmp <- lock_ptn[[Kcount]]
  if (MinimalPartition == TRUE) est_eta <- Gnu[1, tmp]
  else est_eta <- ptn[tmp]
  return(list(est_K = estK, est_z = outz, est_D = estD, est_cp = est_eta, est_Lbd = estLbd, est_pi = estPi))
}

# function to plot the results
ModPlot <- function(Gnu, res, type = "graphs"){
  require(igraph)
  N <- length(res$est_z)
  if (type == "graphs"){
    pal <- brewer.pal(12, 'Paired')
    graphs <- GnuToX(events, res$est_cp, N)
    D <- res$est_D
    for (d in 1:D){
      ig <- graph.adjacency(graphs[,,d], mode = "undirected", weighted = TRUE)
      
      # nodes
      V(ig)$color = res$est_z
      V(ig)$label = " "
      V(ig)$label.color = rgb(0,0,.2,1)
      V(ig)$label.cex = .9
      #V(ig)$size = degree(ig)/2.5 # ou V(g)$size = degree(g) etc, idem pour les edges avec edge betweenness, closeness etc.
      V(ig)$size = 2.5
      V(ig)$frame.color <- V(ig)$frame.color <-rgb(160/255,160/255,160/255,1)
      
      
      #edges
      E(ig)$color <- rgb(204/255,204/255,204/255,.5)
      E(ig)$width <- .8
      E(ig)$arrow.size <- .025
      
      # layout
      if (d == 1) dl <- layout.fruchterman.reingold(ig)
      
      # plotting
      plot(ig, layout=dl, main=paste(" Snapshot ", d))
    }
  }
  if (type == "adjacency"){
    op <- par(bty = "n")
    graphs <- GnuToX(Gnu, res$est_cp, N)
    N <- dim(graphs)[1]
    K <- res$est_K
    for (d in 1:res$est_D){
      I <- graphs[,,d]
      I[I != 0] <- 1
      sZ <- seq(1:N)
      Zclus.copy <- res$est_z
      names(Zclus.copy) <- sZ
      Zclus.copy <- sort(Zclus.copy)
      posZ <- as.numeric(names(Zclus.copy))
      J <- I[posZ, posZ]
      image(t(J)[,N:1], col=gray(255:0/255), useRaster = TRUE, 
                 xaxt = 'n', xlim = c(0,1), ylim = c(0,1), yaxt = 'n', main = paste("Snapshot ", d))  
      # hlines
      lev_hl <- cumsum(rev(table(Zclus.copy)))/N
      lev_hl <- lev_hl[-K]
      clip(0,1,0,1)
      abline(h = lev_hl, col = "red", lwd = 2)
      # vlines
      lev_vl <- cumsum(table(Zclus.copy))/N
      lev_vl <- lev_vl[-K]
      clip(0,1,0,1)
      abline(v = lev_vl, col = "red", lwd = 2)
    }
  }
}
# function to simulate a dataset 
ModSim <- function(Lbd,  # A (K x D) array with Poisson stepwise intensities
                   Pi,   # proportion of nodes to each class
                   eta,  # D positive change points, the last one equal to T
                   N     # number of nodes
){
  K <- nrow(Lbd)
  z <- sample(K, N, replace = TRUE, prob = Pi)
  events <- c()
  for ( i in 1:(N-1) ){
    for (j in (i+1):N){
      tmp <- c()
      for(d in 1:D){
        if (d == 1){
          lbd <- Lbd[z[i],z[j],d]*eta[d]
          Mij <- rpois(1, lbd)
          tmp <- cbind(tmp, matrix(c(sort(runif(Mij, min = 0, max = eta[d])), rep(i, times = Mij), rep(j, times = Mij)), nrow = 3, byrow = TRUE))    
        }
        else{ 
          lbd <- Lbd[z[i], z[j], d]*(eta[d] - eta[d-1])
          Mij <- rpois(1, lbd)
          tmp <- cbind(tmp, matrix(c(sort(runif(Mij, min = eta[d-1], max = eta[d])), rep(i, times = Mij), rep(j, times = Mij)), nrow = 3, byrow = TRUE))    
        }
      }
      events <- cbind(events, tmp) 
    } 
  }
  
  M <- ncol(events)
  pos <- events[1,]
  names(pos) <- c(1:M)
  pos <- sort(pos)
  pos <- as.numeric(names(pos))
  events <- events[,pos]
  
  return(list(events = events, z = z))
}


set.seed(0)


N <- 100                                          # nodes
K <- 3                                            # clusters
D <- 4                                            # time segments
Th <- 10                                          # final time T
up <- 6                                           # two parameters to manange contranst in Lambda
sc <- 0.02                                        
eta <- sort(runif(D-1, 0, Th))                    # change points
eta <- c(eta, Th)                                  
Lbd <- array(dim = c(K,K,D))                      # Poisson intensities
Lbd[,,1] <- sc*1
diag(Lbd[,,1]) <- sc*up
Lbd[,,2] <- sc*1
Lbd[1,1,2] <- Lbd[1,2,2] <- Lbd[2,1,2] <- Lbd[3,3,2] <- sc*up
Lbd[,,3] <- sc*up
diag(Lbd[,,3]) <- sc*1
Lbd[,,4] <- sc*up
Lbd[1,1,4] <- Lbd[1,2,4] <- Lbd[2,1,4] <- Lbd[3,3,4] <- sc*1 
Pi <- rep(1, times = K)                           # proportion to clusters

## -- Simu
out <- ModSim(Lbd, Pi, eta, N)
events <- out$events
z <- out$z

# -- Estim
# Hand made partition 
step <- 0.1
myptn <- seq(from = step, to = Th, by = step)
res <- ModFit(Gnu = events, kmin = 3, kmax = 3, MinimalPartition = FALSE, custom_partition = myptn)

# Minimal partition: it is slower
# res <- ModFit(Gnu = events, kmin = 3, kmax = 3, MinimalPartition = TRUE)

## Looking at results
adjustedRandIndex(res$est_z, z)
res$est_cp
ModPlot(events, res)
ModPlot(events, res, type = "adjacency")


##########  Simulated Data  ################################
###  *** Data Simulation *** ###
# Simulated data - Rossi_Boullé 2012 - Experience A, the first synthetic scenario in the paper 

#set.seed(1)

# Number of nodes in the graph
InitN <- N <- 40

breaks<-c(20,30,60,100)  # The actual change points

# time and node labels
y<-c(rep(1, times=19), rep(2,times=10), rep(3, times=30), rep(4,times=41))
z<-c(rep(1, times=5), rep(2,times=5), rep(3, times=10), rep(4, times=20))

Mtx<-list()
Mtx[[1]]<-diag(c(1,1,1,1), nrow=4, ncol=4)
Mtx[[2]]<-t(matrix(c(0,0,1,1,0,0,0,0,1,0,1,0,1,0,0,0),nrow=4, ncol=4))
Mtx[[3]]<-t(matrix(c(0,1,0,0,1,0,0,0,0,0,0,1,0,0,1,0), nrow=4, ncol=4))
Mtx[[4]]<-t(matrix(c(1,0,0,1,0,1,0,1,0,0,1,1,1,1,1,0), nrow=4, ncol=4))

# simulates the membership (e.g) of i in A (pr(i in A_k)=omega_k) 
find.segment<-function(v, u){
  i<-1
  while (v[i]<u) i=i+1
  return(i)
}

n_edges<-1000

Gnu<-matrix(0, nrow=3, ncol = n_edges)
counter=1
while(counter<= n_edges){
i<-ceiling(runif(1, min=0, max=N))
nu<-runif(1, min = 0, max=100)
d<-find.segment(breaks, nu)
SpeakTo<-which(Mtx[[d]][z[i], ]!=0)   
if (length(SpeakTo)==1){
      store<-which(z==SpeakTo)
      init<-min(store)-1
      end<-max(store)
      j<-ceiling(runif(1, min=init, max=end))
      while (j==i) j<-ceiling(runif(1, min=init, max=end))
      Gnu[,counter]<-c(nu,i,j)
      counter<-counter+1
}
else if (length(SpeakTo)>1){
      store<-which((z %in% SpeakTo)==TRUE)
      end<-length(store)
      tmp<-ceiling(runif(1,0,end))
      j<-store[tmp]
      while (j==i){
        tmp<-ceiling(runif(1,0,end))
        j<-store[tmp]
      }
      Gnu[,counter]<-c(nu,j,i) 
      counter<-counter+1
}
else {}
}
    
str<-which(Gnu[1,]==0);
if (length(str)!=0) print("warning")
#sorting
tmp<-sort(Gnu[1,])
TakeIn<-match(tmp, Gnu[1,])
Gnu<-Gnu[,TakeIn]
    
# undirected interactions (i<j) !!
store<-which(Gnu[2,]>Gnu[3,]) 
Gnu[,store]<-Gnu[c(1,3,2), store]
    
# noising data
taille<-ncol(Gnu);
seuil<-round(.33*taille)
Gnu.copy<-Gnu;
    
mylist<-c()
while (length(mylist)<=seuil){
  tmp<-ceiling(runif(1, 0, (ncol(Gnu.copy))))
  mylist<-c(mylist, Gnu.copy[1,tmp])
  Gnu.copy<-Gnu.copy[,-tmp]
}
    
TakeIn<-match(mylist, Gnu[1,])
for (i in 1:length(TakeIn)){
    at<-TakeIn[i]
    Gnu[3,at]<-ceiling(runif(1,0,N))
    while (Gnu[3,at]==Gnu[2,at]) Gnu[3,at]<-ceiling(runif(1,0,N))
}
    
# making interactions undirected after sampling
store<-which(Gnu[2,]>Gnu[3,]) 
Gnu[,store]<-Gnu[c(1,3,2), store]
    
### *** End Of Simulation *** ###


###
## *** Estimation *** ### 

## function parameters values
MinimalPartition=TRUE
kmin <- 1
kmax <- 5
iter.vem <- 50
N_initializations <- 10
verbose <- TRUE
# -- some examples of (regular) custom partition
# ptn <- c(1:100)
# ptn <- seq(from = 10, to = 100, by = 10)
# ptn <- seq(from = .5, to = 100, by = .5)
# run
res <- ModFit(Gnu, kmin, kmax, N_initializations = 10, 
              MinimalPartition = TRUE, custom_partition=ptn,verbose = TRUE)

wtab.mat <- t(as.matrix(wtab))
row.sample <- sample(1:ncol(wtab.mat), 10000, replace=F)
wtab.M <- wtab.mat[,row.sample]
kmin = 3
kmax = 4
res <- ModFit(wtab.M, kmin, kmax, N_initializations = 10, 
              MinimalPartition = TRUE, custom_partition=NULL,verbose = TRUE)
 
# to check out it worked
cat("adjusted Rand index for node clusters: ", adjustedRandIndex(res$est_z, z), "\n")  # node clustering
cat("true change points: ", breaks, "\n")
cat("estimated change points: ", res$est_cp, "\n")






##########  Real Data ########

# Bike sharing data (London)
load(paste(current_dir, "package/wtab.RData", sep = ""))
load(paste(current_dir, "package/stations_df.RData", sep = ""))

#head(wtab)
#head(stations_df)

# an histogram of the bike hires during the day
par(mfrow=c(1,1), bty="n", cex.axis=1.5, cex.lab=1.5)
hist(wtab$Start.Date, breaks = 60, xaxt="n", yaxt="n", main = "", xlab = "time (hours)")
last <- 86340 # 24h
time.seq<-seq(from=1, to=last, by=3600)  # 86340 is the last element of times
axis(1, at=time.seq, lab=c(0:23), lwd=.2)
axis(2, lwd=.2)

# building the data
Gnu <- as.matrix(wtab)
Gnu <- t(Gnu)
N <- max(Gnu[2,], Gnu[3,])

step <- 900 ## in seconds, correpsonding to 15 minutes
custom_ptn <- seq(from = step, to = max(Gnu[1,]), by = step)

# calling the function
res <- ModFit(Gnu, 11, 11, MinimalPartition = FALSE, custom_partition = custom_ptn, N_initializations = 1,  verbose = TRUE)

## adding change points to the histogram
cp <- res$est_cp
abline(v = cp, col="red", lwd=1.5)


#########*************** plotting stations on London map
library(OpenStreetMap)

new_df <- stations_df[, c(1,2,4,5)]

#converting columns from factor to proper formats
sq<-c(1,3,4)
for(i in 1:length(sq)) new_df[,sq[i]]<-as.numeric(levels(new_df[,sq[i]]))[new_df[,sq[i]]]
new_df[,2]<-as.character(levels(new_df[,2]))[new_df[,2]]

WhoIsWho <- seq(1:N)

match_pos <- res$est_z[match(new_df$id, WhoIsWho)]  # for each station (id) in new_df, i look for its position in WhoIsWho and take outz at this position
new_df<-cbind(new_df, match_pos)
pos_na <- which(is.na(new_df$match_pos))
new_df <- new_df[-pos_na, ]



## fetch map - London
UL <- c(51.450, -0.25)
BR <- c(51.550, 0.02)
London <- openmap(UL,
                  BR,
                  zoom=12,
                  type = "osm")
plot(London)

# managing colors...
tav<-c("RoyalBlue3",  "red", "green3", "blue","cyan","deepskyblue","magenta","black","pink","gold","orangered")
sym<-c(c(1:8),c(12,10,13))

# Add the points layer
sub_df <- new_df[,c(3,4)]
coordinates(sub_df) <- ~long+lat
proj4string(sub_df) <- CRS("+init=epsg:4326")
sub_df <- as.data.frame(spTransform(sub_df, osm()))
points(sub_df$long, sub_df$lat, col=tav[new_df$match_pos], pch=sym[new_df$match_pos],lwd=4)

#########*************** plotting stations on London MAp with mapview (+ facile d'utilisation qu' OpenStreetMap)
library(mapview)
library(sp)


tav<-c("RoyalBlue3","red", "green3", "blue","cyan","deepskyblue","magenta","black","pink","gold","orangered")
sym<-c(c(1:8),c(12,10,13))
sub_df <- new_df[,c(3,4)]
coordinates(sub_df) <- ~long+lat
proj4string(sub_df) <- CRS("+init=epsg:4326")
mapview(sub_df, color=tav[new_df$match_pos], cex= 0.5, alpha = 0.8, lwd=6)




